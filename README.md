# TimeFlip2™ Listener

A simple service that connects to a TimeFlip2™ device via Bluetooth LE and populates time flips via events.

The first approach should consist of:

- scan for TimeFlip2™
- connect to TimeFlip2™
- log current firmware version of TimeFlip2™
- log version update availability
- log flip events
- log battery events

## Later possible Features

- update firmware
- change site configuration
- change led configuration
- change sleep configuration
- …
- change ALL THE configuration!!!

# Required Dependencies

- [tinygo.org/x/bluetooth](https://github.com/tinygo-org/bluetooth)

# Documentation

- [tinygo.org/x/bluetooth sample](https://github.com/tinygo-org/bluetooth/blob/release/examples/scanner/main.go)
- [tinygo.org/x/bluetooth godoc](https://pkg.go.dev/tinygo.org/x/bluetooth)
- [TimeFlip2™ BLE](https://github.com/DI-GROUP/TimeFlip.Docs/tree/master/Hardware)
- [TimeFlip2™ Web API](https://newapi.timeflip.io/swagger-ui.html#/)

